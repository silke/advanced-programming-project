% Division of work over team members and next iteration
% Use package diagrams to present the division of work over the team members.
% Plan the upcoming (i.e. second) cycle rather precisely,
% and give a global plan for the third (and last) cycle.
\section{Planning}
The planning is divided in four phases: proposal, cycle 1, cycle 2 and cycle 3.
Figure~\ref{fig:gantt} shows the planning in 2 hour work days
(14 hours/course/week minus 4 hours of lectures/week),
as \thecourse{} requires one third of the total workload.

\subsection{Proposal}
For the project proposal the idea is formulated and the planning is created.
This proposal leads up to the first milestone.

View the \glhref{tags/proposal}{finished code} on GitLab.

\subsection{Cycle 1}
The first cycle consists of the creation of the bare essentials of the project.

The workload is divided into three categories: integration, vessels and gameplay.

View the \glhref{milestones/1}{milestone} and \glhref{tags/cycle-1}{finished code} on GitLab.

\subsection{Cycle 2}
Because the product delivered in the first cycle is extremely sparse in terms of functionality,
the second cycle will focus on extending functionality and testing.
\glhref{issues?scope=all&state=all&milestone_title=Cycle+2}{A number of issues} (\emph{tasks}) are assigned to the team members. In the second cycle the shortcomings of the first are also improved upon.
These issues are labeled with the
\glhref{issues?scope=all&state=all&milestone_title=Cycle+2&label_name[]=improvement}{\emph{improvement} label}.

View the \glhref{milestones/2}{milestone} and \glhref{tags/cycle-2}{finished code} on GitLab.

\subsection{Cycle 3}
In cycle three, the goal is to achieve a (somewhat) playable game that can be tested.

The following issues (\emph{tasks}) were assigned to the team members:

\begin{itemize}
    \item Jermain:
        % Up to 54
        \begin{itemize}
            \item Create vessel types (\issue{22}):
                Create vessel types like \emph{Battleship} or \emph{Zumwalt class missile destroyer}.
                These have their own characteristics,
                but inherit the behaviour from the \emph{vessel} class.

            \item Resolve the end of turns (\issue{24}):
                at the end of turns the consequences of actions need to be calculated.
                This needs to be done in a comprehensive manner.

            \item Add equipment to vessel (\issue{25}):
                Vessels should have equipment like weapons or engines which affect the damage and movement of the vessel.

            \item Update actions to use actionPoints (\issue{30}):
                Actions are currently \emph{free}, they should cost something.
                When an action is performed its cost should be subtracted from the player's points.

            \item Game: creating a patrol boat creates a destroyer (\issue{39}):
                A simple bug with swapped types.

            \item Modify Game to use equipment to queue actions (\issue{41}):
                Actions should be using equipment to be performed.

            \item Model: Ships need a name string (\issue{42}):
                Every ship type needs a name so it can be shown.

            \item Call resolveActions() at end of all turns (\issue{43}),
                  Properly resolve actions (\issue{44}):
                Actions need to be resolved at the end of \emph{all} turns.

            \item Game: Check if player has lost  (\issue{46}):
                Losing the game is something that 50\% of the players have to experience.
                This needs to be checked in order to make it so.

            \item Make vessel types more distinct (\issue{54}):
                Distinct vessel types add some tactics to the game and makes it more like classic \emph{Battleship}.
        \end{itemize}

    \item Koen:
        % Up to 58
        \begin{itemize}
            \item Modify `view' to support actions (\issue{12}):
                The actions implemented for \issue{11} (above) need to be interfaced with.
                This requires improved communication between the view and the model.
                Actions need be be created, executed and displayed.
                This also requires interaction with the player.

            \item Ship not centred at tiles (\issue{34}):
                Place ships in the centre of tiles.

            \item Change function calls to newItsVessel in Game (\issue{36}):
                Function calls to newItsVessel require a 5th argument (of type char) to specify Vesseltype,
                else it will spawn a FishingBoat instead.
                Use either `d' or `p' as vesselType,
                it is also possible to call the specific functions resp. newVesselDestroyer or newVesselPatrolShip.

            \item Add icons to derived vessel classes (\issue{37}):
                Vessels need to be visually distinct.
                This means they need icons.

            \item View: Show ship info on screen (\issue{38}):
                Ships have stats now. This needs to be shown.

            \item Allow actions to be canceled after they are selected (\issue{45}):
                It's not nice to have to perform an action.
                Especially when the player doesn't have enough action points.

            \item View: Display different types of vessels differently (\issue{49}):
                Use the icons from \issue{37} to show the distinct vessel types.

            \item Make game end states (\issue{50}):
                The game needs to end, which requires a state.

            \item View: Print ship names (\issue{51}):
                Ship names (see \issue{42}) need to be shown.

            \item Add help menu with game instructions (\issue{56}):
                This has been solved by adding controls to options.
                In a later iteration these should be changeable.

            \item View: Show more ship stats when selecting ship (\issue{57}):
                Show the ship data that actually exists in the interface.

            \item Increment action points after turn (\issue{58}):
                Players need more action points to continue playing.
        \end{itemize}

    \item Silke:
        % Up to 55
        \begin{itemize}
            \item Report on `Learning points on iterations (ROPES)' (\issue{33}):
                The report has been updated with explanations of how we have integrated ropes into our workflow and what the up- and downsides of it are.

            \item Update use case diagrams (\issue{35}):
                The use case diagrams need to match the functionality at the end of the cycle.

            \item Remove changed section marking (\issue{40}):
                The helpful marking needs to be removed in order to be added later (see \issue{47}).

            \item Mark changes in report (\issue{47}):
                Changes in the report should be clearly shown again.
                This is no longer required as the other parts are summarised as per the instructions for the report and a discussion with the teaching assistant.

            \item Add issues to report (\issue{52}):
                The issue show a clear overview of how the work was divided.
                It is therefore included in the report.
                Right here to be exact.

            \item Check all code for comments (\issue{55}):
                Unclear code needs to be commented.
                Read through all code to check if it is so.
        \end{itemize}

    \item All: % Unassigned
        % Up to 53
        \begin{itemize}
            \item Summarise unchanged sections (\issue{48}):
                According to the hand-in requirements,
                unchanged information needs to be summarised.

            \item Update design in report (\issue{53}):
                The design of the game has changed and needs to be updated.
        \end{itemize}
\end{itemize}

In the third cycle the shortcomings of the second cycle are also improved upon:

\begin{itemize}
    \item Switch order of use case diagrams and tables (Silke, \issue{31}):
        The order of use case diagrams and use case tables was inverted.
        This was fixed by changing the order.

    \item Fix includes in use case diagrams (Silke, \issue{32}):
        The feedback was that the arrows had the wrong direction.
        A closer look revealed that they were completely wrong.
        The use cases were updated to offer a more comprehensive view of the interaction.

    \item Views: Use polymorphism in view object instantiation (Koen, \issue{59}):
        The objects instantiated for every view should use polymorphism,
        the wanted objects should be instantiated and then stored as a GenericObject in the view classes.
        This way the structure is more object oriented.

        This was not fixed due to two problems:
        \begin{itemize}
            \item Polymorphism is only possible when using pointers which are not generated when using compositions with multiplicity one.
            \item Polymorphism is not useful in this case because we always want an instance of that exact type.
        \end{itemize}
\end{itemize}

View the \glhref{milestones/3}{milestone} and \glhref{tags/cycle-3}{finished code} on GitLab.

\subsection{Cycle X}
The following issues were pushed to future cycles:
\begin{itemize}
    \item Create tests (Silke, \issue{18}):
        create tests within Rhapsody's testing framework to test new functionality.

        This turns out to be a lot of work,
        and does not integrate with our workflow very well.
        It has therefore been postponed until a later iteration.
\end{itemize}

% Planning
\input{gantt}
