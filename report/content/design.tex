% Top-Level Design
% Use class diagrams, and motivate your choices.
% You may also use other diagrams, like State Charts, Sequence Diagrams,
% Collaboration Diagrams, etc.
\section{Top-Level Design}
The software is designed using the \emph{model, view, controller} paradigm\cite{mvc}.
Therefore, the design of the game is divided into three packages: \emph{model}, \emph{view} and \emph{controller}.
The connection between classes in these packages is made via ports.
The \emph{model} and \emph{view} packages contain subpackages to support parallel development.
All of this is shown in figure~\ref{fig:overview:project}).

\begin{figure*}[ht]
    \centering
    \includegraphics{overviews/project}
    \margincaption{Object diagram for the project%
    \label{fig:overview:project}}
\end{figure*}

\subsection{Controller}
The controller provides the interaction between the \emph{view} and the \emph{model} and provides the routing of functionality.
In the state chart for the controller (figure~\ref{fig:statechart:controller}) the general flow of the game state can be seen.

\begin{figure}[ht]
    \centering
    \includegraphics[width=.8\linewidth]{overviews/controller}
    \caption{Object diagram for the controller package}
    \label{fig:overview:controller}
\end{figure}

\begin{figure}[ht]
    \centering
    \includegraphics[width=\linewidth]{statecharts/controller}
    \caption{Object diagram for the Controller class}
    \label{fig:statechart:controller}
\end{figure}

\subsection{Model}
The modelling packages includes the classes used for modelling the behaviour of the game.
All `traditional' elements of the game are included in classes:

\begin{itemize}
    \item \emph{Game}:
        Controls the legal behaviour of the game.
        This is the main modelling class and it interfaces with the \emph{controller}.
        The class contains the game's \emph{players} and \emph{vessels}.
        The game class also contains methods that allow \emph{actions} to be queued and performed at the end of each turn.
    \item \emph{Action}:
		The action class contains the \emph{vessel} and \emph{target coordinates} on which the action should be performed and it has a virtual method to perform the queued action.
		It has two subclasses:
		\begin{itemize}
			\item \emph{Fire}: Implements the \emph{perform} method and extends the class with a \emph{fire power} attribute.
			\item \emph{Move}: Implements the \emph{perform} method.
		\end{itemize}
    \item \emph{Player}:
        Contains the aspects of the player.
        Mainly it's properties like \emph{id} and \emph{name} and it's \emph{action points}.
    \item \emph{Vessel}:
        Contains the aspects of a vessel, properties like \emph{type}, \emph{name}, \emph{health} and \emph{position} and it's \emph{equipment}.
    \item \emph{Equipment}:
		Containts properties like \emph{equipment range} and a virual method to use the \emph{equipment}.
		It has two subclasses:
		\begin{itemize}
			\item \emph{Cannon}(erroneously called Canon): Implements the \emph{use} method and extends the class with a \emph{damage} attribute.
			\item \emph{Engine}: Implements the \emph{use} method.
		\end{itemize}
\end{itemize}

The object model diagram for these classes is shown in figure~\ref{fig:overview:model}, figure~\ref{fig:overview:gameplay} and figure~\ref{fig:overview:vessel}.
Figure~\ref{fig:overview:model} shows the connection between the gameplay package classes and vessel package classes.
Figure~\ref{fig:overview:gameplay} details the gameplay package classes and shows the inheritance of \emph{action}.
Figure~\ref{fig:overview:vessel} details the vessel package classes and shows the inheritance of \emph{equipment} and \emph{vessel}.

\begin{figure}[ht]
    \centering
    \includegraphics[width=\linewidth]{overviews/model}
    \caption{Object model diagram for the model package}
    \label{fig:overview:model}
\end{figure}

\begin{figure}[ht]
    \centering
    \includegraphics[width=.9\linewidth]{overviews/gameplay}
    \caption{Object model diagram for the gameplay package}
    \label{fig:overview:gameplay}
\end{figure}

\begin{figure}[ht]
    \centering
    \includegraphics[width=.9\linewidth]{overviews/vessel}
    \caption{Object model diagram for the vessel package}
    \label{fig:overview:vessel}
\end{figure}

\subsection{View}
The view provides the interfaces that the user interacts with.
At the moment this view is implemented in the \emph{SFMLInterfacePkg}, an object oriented framework for graphical applications.
The object model diagram for this package is shown in figure~\ref{fig:overview:view}.
In the communication between \emph{View} and \emph{Controller} a port is used.
This class uses the available views with a composition relation.
The views are selected based on the controller input and actions taken by the player are returned to the controller.
The controller receives events from the view based on the actions selected by the player on the current view.

All available views are contained within a subpackage: \emph{ViewPkg}.
A composition relation with the main rendering class is required because the state charts of the views are otherwise not updated by the Rhapsody framework.
An example statechart of this is shown in figure~\ref{fig:statechart:menuview}, in this case the main menu.
Views use elements from the \emph{ObjectPkg} to realize the elements that need to be displayed.
Inheritance is used between these objects to reduce duplicate code.

For this cycle, three new classes are added in the objects diagram.
For each ship, a specific view object is added to have a custom shape for each ship.
Polymorphism is used to ensure that the gameplay view only has a list of generic vessel for which only the draw method has to be used to draw the vessel on the board.
This way the code in the GameplayView class only has to be minimally extended to support new vessels.

The gameplay view uses a complex state chart which maps the events generated by user key presses to navigate the command structure.
This statechart is shown in figure~\ref{fig:statechart:gameview}.
The draw method of the gameplay view uses a number of \emph{IS\_IN} macros to check which state the state chart is in.
This way certain objects are only drawn when relevant.
For example, the cursor is only shown when a vessel or a tile needs to be selected.

\begin{figure*}[ht]
    \centering
    \includegraphics[width=.9\linewidth]{overviews/view}
    \margincaption{Object diagram for the view package
    \label{fig:overview:view}}
\end{figure*}

\begin{figure*}[ht]
    \centering
    \includegraphics[width=.7\linewidth]{overviews/viewobjects}
    \margincaption{Object diagram for the objects package
    \label{fig:overview:viewobjects}}
\end{figure*}

\begin{figure}[ht]
    \centering
    \includegraphics[width=.8\linewidth]{statecharts/menuview}
    \caption{State chart for the MenuView class}
    \label{fig:statechart:menuview}
\end{figure}

\begin{figure*}[ht]
    \centering
    \includegraphics[width=.9\linewidth]{statecharts/gameview}
    \margincaption{State chart for the GameView class
    \label{fig:statechart:gameview}}
\end{figure*}
