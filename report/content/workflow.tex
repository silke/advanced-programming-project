\section{Iterative design}
% Includes: Learning points on iterations
Development on the game is done through the ROPES (Rapid Object-oriented Process for Embedded Systems\cite{ropes}) process. This means that the design was split into Cycles.
Ropes defines a \emph{macro-}, \emph{micro-} and \emph{nanocycle}.
In the \emph{\thecourse} course only the iterative process of the micro- and nanocycle were executed as multiple macrocycles would take more time than the course would allow.

The core of the workflow for the group is based on \emph{Git} hosted on \glhref{}{\emph{GitLab} provided by SNT}.
On GitLab, several tasks can be executed:
\begin{itemize}
    \item Version control
    \item Tasks in the form of \emph{issues}
    \item Milestones, which contain issues for each cycle
    \item Automated testing for \emph{Continuous Integration}
\end{itemize}

\subsection{Microcycle}
A microcycle is split into several steps:

\begin{itemize}
    \item Party: \\
          The current features of the project are assessed and the goals of the upcoming cycle clarified.
          Issues that are still pending are moved to the upcoming or later cycle.
          This results in several new \emph{issues} that are assigned to developers.
          These issues can be considered overarching topics that require refinement
    \item Analysis: \\
          In this part the use cases for the current cycle are defined.
          This results in several new \emph{issues} that are assigned to developers.
          Some of these issues are solved immediately.
    \item Design: \\
          The structure of classes and overall hierarchy is discussed.
          This results in several new \emph{issues} that are assigned to developers.
          These issues are often the technical implementation of the issues created in the previous steps and are related to them.
    \item Implementation: \\
          Implementation oriented issues are solved.
          These issues often lead to solving the issues defined in the planning, analysis and design.
    \item Test: \\
          Because of the integrated testing in the nanocycle the code contained in the \emph{master} branch is always tested.
          This means that in this stage only the completion of the use cases is assessed.
\end{itemize}

\subsection{Nanocycle}
A nanocycle, which takes minutes to a few hours, is tightly integrated into the development process with \emph{continuous integration}.
In this context this means that:

\begin{enumerate}
    \item An \emph{issue} is picked to solve.
    \item A \emph{branch} is created for the issue that this nanocycle should solve.
    \item Developers develop code in small parts.
          These small parts are committed and pushed to the Git repository.
    \item After code is pushed to GitLab it is automatically generated and built%
          \footnote{for Linux}.
          If the new code produces errors related to code generation or building it will be detected immediately.
          This means that code is tested \emph{continuously}.
    \item Code is merged into the master branch after review by a separate member.
          As a result, the master branch will always have a working implementation,
          which ensures a reduction of time spent debugging.
          Complex merges are executed with the \emph{Diffmerge} tool that is included with Rhapsody.
    \item The related issue is (automatically) closed.
\end{enumerate}

\subsection{Learning points on iterations}
Using the ROPES model for iterative design in this project has some clear up- and downsides.
Upsides are:

\begin{itemize}
    \item The iterative design, mainly the working in nanocycles, integrates completely with the use of Git and continuous integration.
    \item The focus on delivering a working product at the end of the cycle keeps the development oriented on that goal.
    \item The \emph{party} step ensures that the developers communicate throughout the development of the project.
          This helps the developers keep track of what their colleagues are doing and what the current goals of the project are.
          It also helps developers to talk about the problems they encounter, ensuring common investment in the solving of those problems and ensuring that developers won't stall on similar issues.
\end{itemize}

Downsides are:

\begin{itemize}
    \item As the ROPES method is designed for embedded systems, it contains some steps that are not really present in a Rhapsody (or general C++) project.
          An example is that \emph{design} phase has a tendency to also create the \emph{implementation} for components of the software.
          The same is true for the \emph{testing} phase of which a lot can be automated when working on a software-only project.
    \item Strictly following the ROPES model restricts thinking about the analysis and design when doing the implementation.
          Developers often like to be more flexible, changing things about the project on the fly.
    \item In this project, there is a hard deadline on the end of each microcycle.
          As developers with no prior experience in working with Rhapsody,
          unexpected bugs or behaviour can sometimes cause delays up to the allotted time for the microcycle.
          This also means that a lot of the planned goals of a microcycle might no be reached.
    \item Short microcycles with emphasis on a working product do not reward \emph{groundwork}:
          it is possible to invest quite a lot of work in the model and code without seeing it in the product.
          In this project that can be seen in the second and third cycle:
          the third cycle closes twice the amount of issues with less merge requests as the second cycle.
\end{itemize}

