#!/usr/bin/env bash
set -euf -o pipefail

project="BattleshipDeluxeProExtreme"
team="17"
files="rhapsody"
cycle="$1"
out="$2"
zipflags="-mx=9"

# Typeset LaTeX file
_mktex() {
  # Go to appropriate directory
  cd "report"

  # Typeset
  latexmk -xelatex "report.tex"

  # Go back
  cd -
}

# Create Handin zip
_mkzip() {
  # Name for zip
  zip="${team}-Cy${cycle}-${project}.zip"

  # Delete existing zip
  [[ -e "${zip}" ]] && rm "${zip}"

  # Copy the report to appropriate file
  zrep="${zip/zip/pdf}"
  cp -a "report/report.pdf" "${zrep}"

  # Zip files
  7z a "${zip}" \
    "${zrep}" \
    ${files} \
    ${zipflags}
}

case "${out}" in
  "latex")
    _mktex
    ;;
  "zip")
    _mktex
    _mkzip
    ;;
esac
