#!/bin/sh

license_file="$1"

cat << EOF > /usr/local/bin/rhapsody
#!/bin/sh
export LM_LICENSE_FILE=${license_file}
/opt/IBM/Rhapsody8.1.0/Rhapsody "$\@"
EOF

cat << EOF > /usr/local/bin/rhapsodycl
#!/bin/sh
export LM_LICENSE_FILE=${license_file}
/opt/IBM/Rhapsody8.1.0/RhapsodyCL.exe "\$@"
EOF

cat << EOF > /usr/local/bin/diffmerge
#!/bin/sh
export LM_LICENSE_FILE=${license_file}
/opt/IBM/Rhapsody8.1.0/DiffMerge.exe "\$@"
EOF

chmod +x \
  /usr/local/bin/rhapsody \
  /usr/local/bin/rhapsodycl \
  /usr/local/bin/diffmerge
