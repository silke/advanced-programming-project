I-Logix-RPY-Archive version 8.10.0 C++ 8169320
{ IProject 
	- _id = GUID 5ea4ed1f-38d4-4b93-b96b-e8fa11e8b228;
	- _myState = 8192;
	- _properties = { IPropertyContainer 
		- Subjects = { IRPYRawContainer 
			- size = 4;
			- value = 
			{ IPropertySubject 
				- _Name = "Browser";
				- Metaclasses = { IRPYRawContainer 
					- size = 1;
					- value = 
					{ IPropertyMetaclass 
						- _Name = "Settings";
						- Properties = { IRPYRawContainer 
							- size = 1;
							- value = 
							{ IProperty 
								- _Name = "ShowSourceArtifacts";
								- _Value = "False";
								- _Type = Bool;
							}
						}
					}
				}
			}
			{ IPropertySubject 
				- _Name = "Format";
				- Metaclasses = { IRPYRawContainer 
					- size = 1;
					- value = 
					{ IPropertyMetaclass 
						- _Name = "Class";
						- Properties = { IRPYRawContainer 
							- size = 21;
							- value = 
							{ IProperty 
								- _Name = "DefaultSize";
								- _Value = "0,34,84,148";
								- _Type = String;
							}
							{ IProperty 
								- _Name = "Fill.FillColor";
								- _Value = "255,255,255";
								- _Type = Color;
							}
							{ IProperty 
								- _Name = "Font.Font";
								- _Value = "Tahoma";
								- _Type = String;
							}
							{ IProperty 
								- _Name = "Font.Font@Child.AttributeListCompartment";
								- _Value = "Tahoma";
								- _Type = String;
							}
							{ IProperty 
								- _Name = "Font.Font@Child.OperationListCompartment";
								- _Value = "Tahoma";
								- _Type = String;
							}
							{ IProperty 
								- _Name = "Font.Font@Child.Template";
								- _Value = "Tahoma";
								- _Type = Unknown;
							}
							{ IProperty 
								- _Name = "Font.Italic@Child.AttributeListCompartment";
								- _Value = "0";
								- _Type = Int;
							}
							{ IProperty 
								- _Name = "Font.Italic@Child.OperationListCompartment";
								- _Value = "0";
								- _Type = Int;
							}
							{ IProperty 
								- _Name = "Font.Italic@Child.Template";
								- _Value = "0";
								- _Type = Unknown;
							}
							{ IProperty 
								- _Name = "Font.Size";
								- _Value = "8";
								- _Type = Int;
							}
							{ IProperty 
								- _Name = "Font.Size@Child.AttributeListCompartment";
								- _Value = "8";
								- _Type = Int;
							}
							{ IProperty 
								- _Name = "Font.Size@Child.NameCompartment@Stereotype";
								- _Value = "7";
								- _Type = Int;
							}
							{ IProperty 
								- _Name = "Font.Size@Child.OperationListCompartment";
								- _Value = "8";
								- _Type = Int;
							}
							{ IProperty 
								- _Name = "Font.Size@Child.Template";
								- _Value = "8";
								- _Type = Unknown;
							}
							{ IProperty 
								- _Name = "Font.Weight@Child.AttributeListCompartment";
								- _Value = "400";
								- _Type = Int;
							}
							{ IProperty 
								- _Name = "Font.Weight@Child.NameCompartment@Name";
								- _Value = "700";
								- _Type = Int;
							}
							{ IProperty 
								- _Name = "Font.Weight@Child.OperationListCompartment";
								- _Value = "400";
								- _Type = Int;
							}
							{ IProperty 
								- _Name = "Font.Weight@Child.Template";
								- _Value = "400";
								- _Type = Unknown;
							}
							{ IProperty 
								- _Name = "Line.LineColor";
								- _Value = "109,163,217";
								- _Type = Color;
							}
							{ IProperty 
								- _Name = "Line.LineStyle";
								- _Value = "0";
								- _Type = Int;
							}
							{ IProperty 
								- _Name = "Line.LineWidth";
								- _Value = "1";
								- _Type = Int;
							}
						}
					}
				}
			}
			{ IPropertySubject 
				- _Name = "General";
				- Metaclasses = { IRPYRawContainer 
					- size = 1;
					- value = 
					{ IPropertyMetaclass 
						- _Name = "Model";
						- Properties = { IRPYRawContainer 
							- size = 1;
							- value = 
							{ IProperty 
								- _Name = "ModelCodeAssociativityFineTune";
								- _Value = "Bidirectional";
								- _Type = Enum;
								- _ExtraTypeInfo = "Bidirectional,RoundTrip,CodeGeneration,Never";
							}
						}
					}
				}
			}
			{ IPropertySubject 
				- _Name = "ObjectModelGe";
				- Metaclasses = { IRPYRawContainer 
					- size = 1;
					- value = 
					{ IPropertyMetaclass 
						- _Name = "Class";
						- Properties = { IRPYRawContainer 
							- size = 9;
							- value = 
							{ IProperty 
								- _Name = "Compartments";
								- _Value = "";
								- _Type = MultiLine;
							}
							{ IProperty 
								- _Name = "ShowAttributes";
								- _Value = "All";
								- _Type = Enum;
								- _ExtraTypeInfo = "All,None,Public,Explicit";
							}
							{ IProperty 
								- _Name = "ShowInheritedAttributes";
								- _Value = "False";
								- _Type = Bool;
							}
							{ IProperty 
								- _Name = "ShowInheritedOperations";
								- _Value = "False";
								- _Type = Bool;
							}
							{ IProperty 
								- _Name = "ShowName";
								- _Value = "Relative";
								- _Type = Enum;
								- _ExtraTypeInfo = "Full_path,Relative,Name_only,Label";
							}
							{ IProperty 
								- _Name = "ShowOperations";
								- _Value = "All";
								- _Type = Enum;
								- _ExtraTypeInfo = "All,None,Public,Explicit";
							}
							{ IProperty 
								- _Name = "ShowPorts";
								- _Value = "True";
								- _Type = Bool;
							}
							{ IProperty 
								- _Name = "ShowPortsInterfaces";
								- _Value = "True";
								- _Type = Bool;
							}
							{ IProperty 
								- _Name = "ShowStereotype";
								- _Value = "Label";
								- _Type = Enum;
								- _ExtraTypeInfo = "Label,Bitmap,None";
							}
						}
					}
				}
			}
		}
	}
	- _name = "BattleshipDeluxeProExtreme";
	- _modifiedTimeWeak = 12.20.2016::22:21:2;
	- _lastID = 9;
	- _UserColors = { IRPYRawContainer 
		- size = 16;
		- value = 16777215; 16777215; 16777215; 16777215; 16777215; 16777215; 16777215; 16777215; 16777215; 16777215; 16777215; 16777215; 16777215; 16777215; 16777215; 16777215; 
	}
	- _defaultSubsystem = { ISubsystemHandle 
		- _m2Class = "";
	}
	- _component = { IHandle 
		- _m2Class = "IComponent";
		- _filename = "Test.cmp";
		- _subsystem = "";
		- _class = "";
		- _name = "Test";
		- _id = GUID 5c2d95e6-0d15-4ad0-b2f0-fa2a53b3a0c0;
	}
	- Multiplicities = { IRPYRawContainer 
		- size = 4;
		- value = 
		{ IMultiplicityItem 
			- _name = "1";
			- _count = 7;
		}
		{ IMultiplicityItem 
			- _name = "*";
			- _count = -1;
		}
		{ IMultiplicityItem 
			- _name = "0,1";
			- _count = -1;
		}
		{ IMultiplicityItem 
			- _name = "1..*";
			- _count = -1;
		}
	}
	- Subsystems = { IRPYRawContainer 
		- size = 4;
		- value = 
		{ ISubsystem 
			- fileName = "SFMLInterfacePkg";
			- _id = GUID ac1674aa-f857-45bf-b188-e478b46cba05;
		}
		{ ISubsystem 
			- fileName = "ModelPkg";
			- _id = GUID de5df5df-278d-458c-bc28-c767396caaa6;
		}
		{ ISubsystem 
			- fileName = "ControllerPkg";
			- _id = GUID 2d36f4e1-1cb9-42e8-8e56-5d7dfb7d2be3;
		}
		{ ISubsystem 
			- fileName = "AnalysisPkg";
			- _id = GUID e4bb5f34-6a8c-456c-835c-383cb1ca0f0e;
		}
	}
	- Diagrams = { IRPYRawContainer 
		- size = 1;
		- value = 
		{ IDiagram 
			- _id = GUID 98f0a5da-50f0-476f-9e98-86cc0597da19;
			- _myState = 8192;
			- _properties = { IPropertyContainer 
				- Subjects = { IRPYRawContainer 
					- size = 2;
					- value = 
					{ IPropertySubject 
						- _Name = "Format";
						- Metaclasses = { IRPYRawContainer 
							- size = 6;
							- value = 
							{ IPropertyMetaclass 
								- _Name = "Association";
								- Properties = { IRPYRawContainer 
									- size = 4;
									- value = 
									{ IProperty 
										- _Name = "Font.Font";
										- _Value = "Tahoma";
										- _Type = String;
									}
									{ IProperty 
										- _Name = "Font.Size";
										- _Value = "8";
										- _Type = Int;
									}
									{ IProperty 
										- _Name = "Line.LineColor";
										- _Value = "128,128,128";
										- _Type = Color;
									}
									{ IProperty 
										- _Name = "Line.LineWidth";
										- _Value = "1";
										- _Type = Int;
									}
								}
							}
							{ IPropertyMetaclass 
								- _Name = "Class";
								- Properties = { IRPYRawContainer 
									- size = 21;
									- value = 
									{ IProperty 
										- _Name = "DefaultSize";
										- _Value = "0,34,84,148";
										- _Type = String;
									}
									{ IProperty 
										- _Name = "Fill.FillColor";
										- _Value = "255,255,255";
										- _Type = Color;
									}
									{ IProperty 
										- _Name = "Font.Font";
										- _Value = "Tahoma";
										- _Type = String;
									}
									{ IProperty 
										- _Name = "Font.Font@Child.AttributeListCompartment";
										- _Value = "Tahoma";
										- _Type = String;
									}
									{ IProperty 
										- _Name = "Font.Font@Child.OperationListCompartment";
										- _Value = "Tahoma";
										- _Type = String;
									}
									{ IProperty 
										- _Name = "Font.Font@Child.Template Params";
										- _Value = "Tahoma";
										- _Type = String;
									}
									{ IProperty 
										- _Name = "Font.Italic@Child.AttributeListCompartment";
										- _Value = "0";
										- _Type = Int;
									}
									{ IProperty 
										- _Name = "Font.Italic@Child.OperationListCompartment";
										- _Value = "0";
										- _Type = Int;
									}
									{ IProperty 
										- _Name = "Font.Italic@Child.Template Params";
										- _Value = "0";
										- _Type = Int;
									}
									{ IProperty 
										- _Name = "Font.Size";
										- _Value = "8";
										- _Type = Int;
									}
									{ IProperty 
										- _Name = "Font.Size@Child.AttributeListCompartment";
										- _Value = "8";
										- _Type = Int;
									}
									{ IProperty 
										- _Name = "Font.Size@Child.NameCompartment@Stereotype";
										- _Value = "7";
										- _Type = Int;
									}
									{ IProperty 
										- _Name = "Font.Size@Child.OperationListCompartment";
										- _Value = "8";
										- _Type = Int;
									}
									{ IProperty 
										- _Name = "Font.Size@Child.Template Params";
										- _Value = "8";
										- _Type = Int;
									}
									{ IProperty 
										- _Name = "Font.Weight@Child.AttributeListCompartment";
										- _Value = "400";
										- _Type = Int;
									}
									{ IProperty 
										- _Name = "Font.Weight@Child.NameCompartment@Name";
										- _Value = "700";
										- _Type = Int;
									}
									{ IProperty 
										- _Name = "Font.Weight@Child.OperationListCompartment";
										- _Value = "400";
										- _Type = Int;
									}
									{ IProperty 
										- _Name = "Font.Weight@Child.Template Params";
										- _Value = "400";
										- _Type = Int;
									}
									{ IProperty 
										- _Name = "Line.LineColor";
										- _Value = "109,163,217";
										- _Type = Color;
									}
									{ IProperty 
										- _Name = "Line.LineStyle";
										- _Value = "0";
										- _Type = Int;
									}
									{ IProperty 
										- _Name = "Line.LineWidth";
										- _Value = "1";
										- _Type = Int;
									}
								}
							}
							{ IPropertyMetaclass 
								- _Name = "Composition";
								- Properties = { IRPYRawContainer 
									- size = 4;
									- value = 
									{ IProperty 
										- _Name = "Font.Font";
										- _Value = "Tahoma";
										- _Type = String;
									}
									{ IProperty 
										- _Name = "Font.Size";
										- _Value = "8";
										- _Type = Int;
									}
									{ IProperty 
										- _Name = "Line.LineColor";
										- _Value = "128,128,128";
										- _Type = Color;
									}
									{ IProperty 
										- _Name = "Line.LineWidth";
										- _Value = "1";
										- _Type = Int;
									}
								}
							}
							{ IPropertyMetaclass 
								- _Name = "Inheritance";
								- Properties = { IRPYRawContainer 
									- size = 4;
									- value = 
									{ IProperty 
										- _Name = "Font.Font";
										- _Value = "Tahoma";
										- _Type = String;
									}
									{ IProperty 
										- _Name = "Font.Size";
										- _Value = "8";
										- _Type = Int;
									}
									{ IProperty 
										- _Name = "Line.LineColor";
										- _Value = "128,128,128";
										- _Type = Color;
									}
									{ IProperty 
										- _Name = "Line.LineWidth";
										- _Value = "1";
										- _Type = Int;
									}
								}
							}
							{ IPropertyMetaclass 
								- _Name = "Package";
								- Properties = { IRPYRawContainer 
									- size = 7;
									- value = 
									{ IProperty 
										- _Name = "DefaultSize";
										- _Value = "0,0,216,151";
										- _Type = String;
									}
									{ IProperty 
										- _Name = "Fill.FillColor";
										- _Value = "255,255,255";
										- _Type = Color;
									}
									{ IProperty 
										- _Name = "Font.Font";
										- _Value = "Tahoma";
										- _Type = String;
									}
									{ IProperty 
										- _Name = "Font.Size";
										- _Value = "8";
										- _Type = Int;
									}
									{ IProperty 
										- _Name = "Font.Weight@Child.NameCompartment@Name";
										- _Value = "700";
										- _Type = Int;
									}
									{ IProperty 
										- _Name = "Line.LineColor";
										- _Value = "109,163,217";
										- _Type = Color;
									}
									{ IProperty 
										- _Name = "Line.LineWidth";
										- _Value = "1";
										- _Type = Int;
									}
								}
							}
							{ IPropertyMetaclass 
								- _Name = "Port";
								- Properties = { IRPYRawContainer 
									- size = 7;
									- value = 
									{ IProperty 
										- _Name = "DefaultSize";
										- _Value = "0,0,68,73";
										- _Type = String;
									}
									{ IProperty 
										- _Name = "Fill.FillColor";
										- _Value = "255,255,255";
										- _Type = Color;
									}
									{ IProperty 
										- _Name = "Font.Font";
										- _Value = "Tahoma";
										- _Type = String;
									}
									{ IProperty 
										- _Name = "Font.Size";
										- _Value = "8";
										- _Type = Int;
									}
									{ IProperty 
										- _Name = "Font.Weight@Child.NameCompartment@Name";
										- _Value = "700";
										- _Type = Int;
									}
									{ IProperty 
										- _Name = "Line.LineColor";
										- _Value = "109,163,217";
										- _Type = Color;
									}
									{ IProperty 
										- _Name = "Line.LineWidth";
										- _Value = "1";
										- _Type = Int;
									}
								}
							}
						}
					}
					{ IPropertySubject 
						- _Name = "General";
						- Metaclasses = { IRPYRawContainer 
							- size = 1;
							- value = 
							{ IPropertyMetaclass 
								- _Name = "Graphics";
								- Properties = { IRPYRawContainer 
									- size = 1;
									- value = 
									{ IProperty 
										- _Name = "grid_snap";
										- _Value = "True";
										- _Type = Bool;
									}
								}
							}
						}
					}
				}
			}
			- _name = "ProjectOverview";
			- _modifiedTimeWeak = 1.2.1990::0:0:0;
			- _lastModifiedTime = "12.20.2016::22:28:6";
			- _graphicChart = { CGIClassChart 
				- _id = GUID 79cfe0d1-af1f-4d17-b8de-9297ddb332ab;
				- m_type = 0;
				- m_pModelObject = { IHandle 
					- _m2Class = "IDiagram";
					- _id = GUID 98f0a5da-50f0-476f-9e98-86cc0597da19;
				}
				- m_pParent = ;
				- m_name = { CGIText 
					- m_str = "";
					- m_style = "Arial" 10 0 0 0 1 ;
					- m_color = { IColor 
						- m_fgColor = 0;
						- m_bgColor = 0;
						- m_bgFlag = 0;
					}
					- m_position = 1 0 0  ;
					- m_nIdent = 0;
					- m_bImplicitSetRectPoints = 0;
					- m_nOrientationCtrlPt = 8;
				}
				- m_drawBehavior = 0;
				- m_bIsPreferencesInitialized = 0;
				- elementList = 16;
				{ CGIClass 
					- _id = GUID ebdfea3a-d71e-485f-8ceb-2e6e1cd725f9;
					- m_type = 78;
					- m_pModelObject = { IHandle 
						- _m2Class = "";
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "TopLevel";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 5;
					}
					- m_drawBehavior = 0;
					- m_bIsPreferencesInitialized = 0;
					- m_AdditionalLabel = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 1;
					}
					- m_polygon = 0 ;
					- m_nNameFormat = 0;
					- m_nIsNameFormat = 0;
					- Compartments = { IRPYRawContainer 
						- size = 2;
						- value = 
						{ CGICompartment 
							- _id = GUID c9919fd1-733d-4b0b-9027-e3838b2f977c;
							- m_name = "Attribute";
							- m_displayOption = Explicit;
							- m_bShowInherited = 0;
							- m_bOrdered = 0;
							- Items = { IRPYRawContainer 
								- size = 0;
							}
						}
						{ CGICompartment 
							- _id = GUID 2737bf23-40d4-4b03-a803-e55856bfa85c;
							- m_name = "Operation";
							- m_displayOption = Explicit;
							- m_bShowInherited = 0;
							- m_bOrdered = 0;
							- Items = { IRPYRawContainer 
								- size = 0;
							}
						}
					}
					- Attrs = { IRPYRawContainer 
						- size = 0;
					}
					- Operations = { IRPYRawContainer 
						- size = 0;
					}
				}
				{ CGIClass 
					- _id = GUID 7eb51212-3a80-4c2e-864e-0ef65ea6efe8;
					- _properties = { IPropertyContainer 
						- Subjects = { IRPYRawContainer 
							- size = 1;
							- value = 
							{ IPropertySubject 
								- _Name = "General";
								- Metaclasses = { IRPYRawContainer 
									- size = 1;
									- value = 
									{ IPropertyMetaclass 
										- _Name = "Graphics";
										- Properties = { IRPYRawContainer 
											- size = 1;
											- value = 
											{ IProperty 
												- _Name = "FitBoxToItsTextuals";
												- _Value = "False";
												- _Type = Bool;
											}
										}
									}
								}
							}
						}
					}
					- m_type = 87;
					- m_pModelObject = { IHandle 
						- _m2Class = "IClass";
						- _filename = "ControllerPkg.sbs";
						- _subsystem = "ControllerPkg";
						- _class = "";
						- _name = "Controller";
						- _id = GUID 3e8c88a4-dbe5-4518-86ea-aea7621a7181;
					}
					- m_pParent = GUID 3426b9d4-e23f-4fd8-ab7a-79293b69d139;
					- m_name = { CGIText 
						- m_str = "Controller";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 5;
					}
					- m_drawBehavior = 2056;
					- m_transform = 0.761386 0 0 0.762736 50.7801 -146.256 ;
					- m_bIsPreferencesInitialized = 1;
					- m_AdditionalLabel = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 1;
					}
					- m_bFramesetModified = 1;
					- m_polygon = 4 2 329  2 1451  1061 1451  1061 329  ;
					- m_nNameFormat = 0;
					- m_nIsNameFormat = 0;
					- frameset = "<frameset rows=10%,90%>
<frame name=AttributeListCompartment>
<frame name=OperationListCompartment>";
					- Compartments = { IRPYRawContainer 
						- size = 2;
						- value = 
						{ CGICompartment 
							- _id = GUID 46a1d47c-0303-45c9-9ff8-3a4b7b89f7ca;
							- m_name = "Attribute";
							- m_displayOption = Explicit;
							- m_bShowInherited = 0;
							- m_bOrdered = 0;
							- Items = { IRPYRawContainer 
								- size = 0;
							}
						}
						{ CGICompartment 
							- _id = GUID 6a29b1f7-f3e8-4ae9-a435-25590ccdcbff;
							- m_name = "Operation";
							- m_displayOption = Explicit;
							- m_bShowInherited = 0;
							- m_bOrdered = 1;
							- Items = { IRPYRawContainer 
								- size = 13;
								- value = 
								{ IHandle 
									- _m2Class = "IReception";
									- _filename = "ControllerPkg.sbs";
									- _subsystem = "ControllerPkg";
									- _class = "Controller";
									- _name = "evFlowBack()";
									- _id = GUID 4b1aa67d-315b-4ede-b509-6b34f70cbf01;
								}
								{ IHandle 
									- _m2Class = "IReception";
									- _filename = "ControllerPkg.sbs";
									- _subsystem = "ControllerPkg";
									- _class = "Controller";
									- _name = "evFlowCredits()";
									- _id = GUID c50e2661-2665-4510-83e2-4e6238c08496;
								}
								{ IHandle 
									- _m2Class = "IReception";
									- _filename = "ControllerPkg.sbs";
									- _subsystem = "ControllerPkg";
									- _class = "Controller";
									- _name = "evFlowEndTurn()";
									- _id = GUID 0e93941c-2079-4220-a028-7b79bd33042a;
								}
								{ IHandle 
									- _m2Class = "IReception";
									- _filename = "ControllerPkg.sbs";
									- _subsystem = "ControllerPkg";
									- _class = "Controller";
									- _name = "evFlowOptions()";
									- _id = GUID c6cfe6ab-74dd-4bbf-9fd8-34bb9ef6a194;
								}
								{ IHandle 
									- _m2Class = "IReception";
									- _filename = "ControllerPkg.sbs";
									- _subsystem = "ControllerPkg";
									- _class = "Controller";
									- _name = "evFlowQuitGame()";
									- _id = GUID d55e68d6-b939-4de0-a5db-82336c874755;
								}
								{ IHandle 
									- _m2Class = "IReception";
									- _filename = "ControllerPkg.sbs";
									- _subsystem = "ControllerPkg";
									- _class = "Controller";
									- _name = "evFlowShutdown()";
									- _id = GUID 392bccf7-4a57-4e3b-8e60-78162ca144ff;
								}
								{ IHandle 
									- _m2Class = "IReception";
									- _filename = "ControllerPkg.sbs";
									- _subsystem = "ControllerPkg";
									- _class = "Controller";
									- _name = "evFlowStartGame()";
									- _id = GUID 6b0bb323-9d82-4d8e-b843-f0849315ca50;
								}
								{ IHandle 
									- _m2Class = "IReception";
									- _filename = "ControllerPkg.sbs";
									- _subsystem = "ControllerPkg";
									- _class = "Controller";
									- _name = "evFlowSwitchPlayer()";
									- _id = GUID 7c642622-8f2c-41cc-8df3-6d68c76c51e5;
								}
								{ IHandle 
									- _m2Class = "IPrimitiveOperation";
									- _filename = "ControllerPkg.sbs";
									- _subsystem = "ControllerPkg";
									- _class = "Controller";
									- _name = "getActivePlayer()";
									- _id = GUID 36e113ad-f349-472e-b7bd-795efde2d3e9;
								}
								{ IHandle 
									- _m2Class = "IPrimitiveOperation";
									- _filename = "ControllerPkg.sbs";
									- _subsystem = "ControllerPkg";
									- _class = "Controller";
									- _name = "getTurnCount()";
									- _id = GUID 975876b5-b9f6-40a4-b9ad-0e4f03c2efab;
								}
								{ IHandle 
									- _m2Class = "IPrimitiveOperation";
									- _filename = "ControllerPkg.sbs";
									- _subsystem = "ControllerPkg";
									- _class = "Controller";
									- _name = "initialize()";
									- _id = GUID d05b34d8-6792-4f59-b86a-dd669e03851b;
								}
								{ IHandle 
									- _m2Class = "IPrimitiveOperation";
									- _filename = "ControllerPkg.sbs";
									- _subsystem = "ControllerPkg";
									- _class = "Controller";
									- _name = "nextTurn()";
									- _id = GUID a6210564-0a64-4623-8503-c09838c2c49d;
								}
								{ IHandle 
									- _m2Class = "IPrimitiveOperation";
									- _filename = "ControllerPkg.sbs";
									- _subsystem = "ControllerPkg";
									- _class = "Controller";
									- _name = "setupPlayers()";
									- _id = GUID 5f8f7524-63ba-4d76-8200-82c4da8ae696;
								}
							}
						}
					}
					- Attrs = { IRPYRawContainer 
						- size = 0;
					}
					- Operations = { IRPYRawContainer 
						- size = 0;
					}
				}
				{ CGIClass 
					- _id = GUID 2120b671-3371-4b68-9b45-f826c8238cb2;
					- _properties = { IPropertyContainer 
						- Subjects = { IRPYRawContainer 
							- size = 1;
							- value = 
							{ IPropertySubject 
								- _Name = "General";
								- Metaclasses = { IRPYRawContainer 
									- size = 1;
									- value = 
									{ IPropertyMetaclass 
										- _Name = "Graphics";
										- Properties = { IRPYRawContainer 
											- size = 1;
											- value = 
											{ IProperty 
												- _Name = "FitBoxToItsTextuals";
												- _Value = "False";
												- _Type = Bool;
											}
										}
									}
								}
							}
						}
					}
					- m_type = 87;
					- m_pModelObject = { IHandle 
						- _m2Class = "IClass";
						- _filename = "SFMLInterfacePkg.sbs";
						- _subsystem = "SFMLInterfacePkg";
						- _class = "";
						- _name = "InterfaceManager";
						- _id = GUID 2aac598c-5e1d-41f9-b35f-0f9b91b85858;
					}
					- m_pParent = GUID 682782bf-e35b-4bc7-a416-2abcf7467f12;
					- m_name = { CGIText 
						- m_str = "InterfaceManager";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 5;
					}
					- m_drawBehavior = 2056;
					- m_transform = 0.404608 0 0 0.720657 145.19 -142.085 ;
					- m_bIsPreferencesInitialized = 1;
					- m_AdditionalLabel = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 1;
					}
					- m_bFramesetModified = 1;
					- m_polygon = 4 2 329  2 1451  1061 1451  1061 329  ;
					- m_nNameFormat = 0;
					- m_nIsNameFormat = 0;
					- frameset = "<frameset rows=10%,90%>
<frame name=AttributeListCompartment>
<frame name=OperationListCompartment>";
					- Compartments = { IRPYRawContainer 
						- size = 2;
						- value = 
						{ CGICompartment 
							- _id = GUID 3e649edc-540b-4f3c-9e68-bd00536a11a7;
							- m_name = "Attribute";
							- m_displayOption = Explicit;
							- m_bShowInherited = 0;
							- m_bOrdered = 0;
							- Items = { IRPYRawContainer 
								- size = 0;
							}
						}
						{ CGICompartment 
							- _id = GUID b6df616b-31e9-425a-835e-e4d9257d1359;
							- m_name = "Operation";
							- m_displayOption = Explicit;
							- m_bShowInherited = 0;
							- m_bOrdered = 1;
							- Items = { IRPYRawContainer 
								- size = 12;
								- value = 
								{ IHandle 
									- _m2Class = "IPrimitiveOperation";
									- _filename = "SFMLInterfacePkg.sbs";
									- _subsystem = "SFMLInterfacePkg";
									- _class = "InterfaceManager";
									- _name = "destroyWindow()";
									- _id = GUID bc953626-5f2b-40bb-b39e-e5502466faea;
								}
								{ IHandle 
									- _m2Class = "IReception";
									- _filename = "SFMLInterfacePkg.sbs";
									- _subsystem = "SFMLInterfacePkg";
									- _class = "InterfaceManager";
									- _name = "evCreateDisplay()";
									- _id = GUID de22d4b6-82f8-4208-8df0-7a99fee36946;
								}
								{ IHandle 
									- _m2Class = "IReception";
									- _filename = "SFMLInterfacePkg.sbs";
									- _subsystem = "SFMLInterfacePkg";
									- _class = "InterfaceManager";
									- _name = "evDestroyDisplay()";
									- _id = GUID be778d55-6fb5-452d-8856-bc2961d4bea6;
								}
								{ IHandle 
									- _m2Class = "IReception";
									- _filename = "SFMLInterfacePkg.sbs";
									- _subsystem = "SFMLInterfacePkg";
									- _class = "InterfaceManager";
									- _name = "evSetNextView()";
									- _id = GUID 2259c3b0-70f5-4af8-b3dc-47a3e4811073;
								}
								{ IHandle 
									- _m2Class = "IPrimitiveOperation";
									- _filename = "SFMLInterfacePkg.sbs";
									- _subsystem = "SFMLInterfacePkg";
									- _class = "InterfaceManager";
									- _name = "getActivePlayer()";
									- _id = GUID 77d7d3e7-33cd-4c94-99dd-113c3912ec63;
								}
								{ IHandle 
									- _m2Class = "IPrimitiveOperation";
									- _filename = "SFMLInterfacePkg.sbs";
									- _subsystem = "SFMLInterfacePkg";
									- _class = "InterfaceManager";
									- _name = "getTurnCount()";
									- _id = GUID 45ba62e2-0869-40f7-aa88-a60267327e71;
								}
								{ IHandle 
									- _m2Class = "IPrimitiveOperation";
									- _filename = "SFMLInterfacePkg.sbs";
									- _subsystem = "SFMLInterfacePkg";
									- _class = "InterfaceManager";
									- _name = "getVesselList()";
									- _id = GUID 94c0c051-e425-42f3-8c3f-5a520ed0dd29;
								}
								{ IHandle 
									- _m2Class = "IPrimitiveOperation";
									- _filename = "SFMLInterfacePkg.sbs";
									- _subsystem = "SFMLInterfacePkg";
									- _class = "InterfaceManager";
									- _name = "handleEvent()";
									- _id = GUID ae9b0cb7-a3c5-405a-88fd-c740d5dede58;
								}
								{ IHandle 
									- _m2Class = "IPrimitiveOperation";
									- _filename = "SFMLInterfacePkg.sbs";
									- _subsystem = "SFMLInterfacePkg";
									- _class = "InterfaceManager";
									- _name = "renderLoop()";
									- _id = GUID 4a909412-dcf5-4f53-87a0-c03c6d7910a1;
								}
								{ IHandle 
									- _m2Class = "IPrimitiveOperation";
									- _filename = "SFMLInterfacePkg.sbs";
									- _subsystem = "SFMLInterfacePkg";
									- _class = "InterfaceManager";
									- _name = "sendFlow(IOxfEvent*)";
									- _id = GUID 0049fe04-9837-4894-a6eb-d412d7337eb8;
								}
								{ IHandle 
									- _m2Class = "IPrimitiveOperation";
									- _filename = "SFMLInterfacePkg.sbs";
									- _subsystem = "SFMLInterfacePkg";
									- _class = "InterfaceManager";
									- _name = "setupWindow()";
									- _id = GUID bd015f76-c8f2-4643-9a75-a76ed3b3db9b;
								}
								{ IHandle 
									- _m2Class = "IPrimitiveOperation";
									- _filename = "SFMLInterfacePkg.sbs";
									- _subsystem = "SFMLInterfacePkg";
									- _class = "InterfaceManager";
									- _name = "updateCurrentView()";
									- _id = GUID f1b49e1f-d8dd-4daa-974d-58387bb869a0;
								}
							}
						}
					}
					- Attrs = { IRPYRawContainer 
						- size = 0;
					}
					- Operations = { IRPYRawContainer 
						- size = 0;
					}
				}
				{ CGIPortConnector 
					- _id = GUID 932c4308-700e-4c9a-b630-00ac2c665ef9;
					- m_type = 57;
					- m_pModelObject = { IHandle 
						- _m2Class = "IPort";
						- _filename = "ControllerPkg.sbs";
						- _subsystem = "ControllerPkg";
						- _class = "Controller";
						- _name = "ui";
						- _id = GUID 36b0bbfe-df8c-48b9-a976-9f3339dcd46c;
					}
					- m_pParent = GUID 7eb51212-3a80-4c2e-864e-0ef65ea6efe8;
					- m_name = { CGIText 
						- m_str = "ui";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nHorizontalSpacing = -12;
						- m_nVerticalSpacing = 9;
						- m_nOrientationCtrlPt = 1;
					}
					- m_drawBehavior = 0;
					- m_transform = -5.72432 0 -0 -3.85943 692.643 1596.43 ;
					- m_bIsPreferencesInitialized = 0;
					- m_AdditionalLabel = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nHorizontalSpacing = -7;
						- m_nVerticalSpacing = -4;
						- m_nOrientationCtrlPt = 1;
					}
					- m_position = 4 0 0  0 73  68 73  68 0  ;
					- m_pInheritsFrom = { IHandle 
						- _m2Class = "";
					}
					- m_nInheritanceMask = 0;
					- m_SubType = 0;
					- m_providedInterfaceLabel = { CGIText 
						- m_str = "IController";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nHorizontalSpacing = -40;
						- m_nVerticalSpacing = 19;
						- m_nOrientationCtrlPt = 6;
					}
					- m_requiredInterfaceLabel = { CGIText 
						- m_str = "IUIController";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nHorizontalSpacing = 47;
						- m_nVerticalSpacing = 19;
						- m_nOrientationCtrlPt = 4;
					}
				}
				{ CGIPortConnector 
					- _id = GUID bbca3dc1-b9ec-41e9-9a41-dcb2298c2d06;
					- m_type = 57;
					- m_pModelObject = { IHandle 
						- _m2Class = "IPort";
						- _filename = "SFMLInterfacePkg.sbs";
						- _subsystem = "SFMLInterfacePkg";
						- _class = "InterfaceManager";
						- _name = "ui";
						- _id = GUID e17e835d-0623-4846-9a7f-0e5a4176f709;
					}
					- m_pParent = GUID 2120b671-3371-4b68-9b45-f826c8238cb2;
					- m_name = { CGIText 
						- m_str = "ui";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nHorizontalSpacing = -12;
						- m_nVerticalSpacing = 9;
						- m_nOrientationCtrlPt = 1;
					}
					- m_drawBehavior = 0;
					- m_transform = -6.95685 0 -0 -4.08482 763.458 1603.97 ;
					- m_bIsPreferencesInitialized = 0;
					- m_AdditionalLabel = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nHorizontalSpacing = -7;
						- m_nVerticalSpacing = -4;
						- m_nOrientationCtrlPt = 1;
					}
					- m_position = 4 0 0  0 73  68 73  68 0  ;
					- m_pInheritsFrom = { IHandle 
						- _m2Class = "";
					}
					- m_nInheritanceMask = 0;
					- m_SubType = 0;
					- m_providedInterfaceLabel = { CGIText 
						- m_str = "IUIController";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nHorizontalSpacing = -47;
						- m_nVerticalSpacing = 19;
						- m_nOrientationCtrlPt = 6;
					}
					- m_requiredInterfaceLabel = { CGIText 
						- m_str = "IController";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nHorizontalSpacing = 40;
						- m_nVerticalSpacing = 19;
						- m_nOrientationCtrlPt = 4;
					}
				}
				{ CGIPackage 
					- _id = GUID 3426b9d4-e23f-4fd8-ab7a-79293b69d139;
					- _properties = { IPropertyContainer 
						- Subjects = { IRPYRawContainer 
							- size = 1;
							- value = 
							{ IPropertySubject 
								- _Name = "General";
								- Metaclasses = { IRPYRawContainer 
									- size = 1;
									- value = 
									{ IPropertyMetaclass 
										- _Name = "Graphics";
										- Properties = { IRPYRawContainer 
											- size = 1;
											- value = 
											{ IProperty 
												- _Name = "FitBoxToItsTextuals";
												- _Value = "False";
												- _Type = Bool;
											}
										}
									}
								}
							}
						}
					}
					- m_type = 127;
					- m_pModelObject = { IHandle 
						- _m2Class = "ISubsystem";
						- _filename = "ControllerPkg.sbs";
						- _subsystem = "";
						- _class = "";
						- _name = "ControllerPkg";
						- _id = GUID 2d36f4e1-1cb9-42e8-8e56-5d7dfb7d2be3;
					}
					- m_pParent = GUID ebdfea3a-d71e-485f-8ceb-2e6e1cd725f9;
					- m_name = { CGIText 
						- m_str = "ControllerPkg";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 5;
					}
					- m_drawBehavior = 4104;
					- m_transform = 0.229441 0 0 0.339705 36 17.1135 ;
					- m_bIsPreferencesInitialized = 1;
					- m_AdditionalLabel = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 1;
					}
					- m_polygon = 4 0 0  0 1151  1216 1151  1216 0  ;
					- m_nNameFormat = 0;
					- m_nIsNameFormat = 0;
					- Compartments = { IRPYRawContainer 
						- size = 0;
					}
				}
				{ CGIPackage 
					- _id = GUID dca863ed-7e31-4997-a9ef-7555a6fe1110;
					- _properties = { IPropertyContainer 
						- Subjects = { IRPYRawContainer 
							- size = 1;
							- value = 
							{ IPropertySubject 
								- _Name = "General";
								- Metaclasses = { IRPYRawContainer 
									- size = 1;
									- value = 
									{ IPropertyMetaclass 
										- _Name = "Graphics";
										- Properties = { IRPYRawContainer 
											- size = 1;
											- value = 
											{ IProperty 
												- _Name = "FitBoxToItsTextuals";
												- _Value = "False";
												- _Type = Bool;
											}
										}
									}
								}
							}
						}
					}
					- m_type = 127;
					- m_pModelObject = { IHandle 
						- _m2Class = "ISubsystem";
						- _filename = "ModelPkg.sbs";
						- _subsystem = "";
						- _class = "";
						- _name = "ModelPkg";
						- _id = GUID de5df5df-278d-458c-bc28-c767396caaa6;
					}
					- m_pParent = GUID ebdfea3a-d71e-485f-8ceb-2e6e1cd725f9;
					- m_name = { CGIText 
						- m_str = "ModelPkg";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 5;
					}
					- m_drawBehavior = 4104;
					- m_transform = 0.226974 0 0 0.339705 324 16.6253 ;
					- m_bIsPreferencesInitialized = 1;
					- m_AdditionalLabel = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 1;
					}
					- m_polygon = 4 0 0  0 1151  1216 1151  1216 0  ;
					- m_nNameFormat = 0;
					- m_nIsNameFormat = 0;
					- Compartments = { IRPYRawContainer 
						- size = 0;
					}
				}
				{ CGIPackage 
					- _id = GUID 682782bf-e35b-4bc7-a416-2abcf7467f12;
					- _properties = { IPropertyContainer 
						- Subjects = { IRPYRawContainer 
							- size = 1;
							- value = 
							{ IPropertySubject 
								- _Name = "General";
								- Metaclasses = { IRPYRawContainer 
									- size = 1;
									- value = 
									{ IPropertyMetaclass 
										- _Name = "Graphics";
										- Properties = { IRPYRawContainer 
											- size = 1;
											- value = 
											{ IProperty 
												- _Name = "FitBoxToItsTextuals";
												- _Value = "False";
												- _Type = Bool;
											}
										}
									}
								}
							}
						}
					}
					- m_type = 127;
					- m_pModelObject = { IHandle 
						- _m2Class = "ISubsystem";
						- _filename = "SFMLInterfacePkg.sbs";
						- _subsystem = "";
						- _class = "";
						- _name = "SFMLInterfacePkg";
						- _id = GUID ac1674aa-f857-45bf-b188-e478b46cba05;
					}
					- m_pParent = GUID ebdfea3a-d71e-485f-8ceb-2e6e1cd725f9;
					- m_name = { CGIText 
						- m_str = "SFMLInterfacePkg";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 5;
					}
					- m_drawBehavior = 4104;
					- m_transform = 0.355263 0 0 0.339704 612 16.3698 ;
					- m_bIsPreferencesInitialized = 1;
					- m_AdditionalLabel = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 1;
					}
					- m_polygon = 4 0 0  0 1151  1216 1151  1216 0  ;
					- m_nNameFormat = 0;
					- m_nIsNameFormat = 0;
					- Compartments = { IRPYRawContainer 
						- size = 0;
					}
				}
				{ CGIPackage 
					- _id = GUID 49ac931f-4968-4931-8298-9c9ac4a9a464;
					- m_type = 127;
					- m_pModelObject = { IHandle 
						- _m2Class = "ISubsystem";
						- _filename = "GameplayPkg.sbs";
						- _subsystem = "ModelPkg";
						- _class = "";
						- _name = "GameplayPkg";
						- _id = GUID 6f776762-1714-40e3-937d-75497e9ed7df;
					}
					- m_pParent = GUID dca863ed-7e31-4997-a9ef-7555a6fe1110;
					- m_name = { CGIText 
						- m_str = "GameplayPkg";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 5;
					}
					- m_drawBehavior = 4104;
					- m_transform = 0.869794 0 0 0.585549 53.4594 77.9612 ;
					- m_bIsPreferencesInitialized = 1;
					- m_AdditionalLabel = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 1;
					}
					- m_polygon = 4 0 0  0 1151  1216 1151  1216 0  ;
					- m_nNameFormat = 0;
					- m_nIsNameFormat = 0;
					- Compartments = { IRPYRawContainer 
						- size = 0;
					}
				}
				{ CGIClass 
					- _id = GUID d83deca8-4ed2-4ddd-b2e0-77ceb4482862;
					- _properties = { IPropertyContainer 
						- Subjects = { IRPYRawContainer 
							- size = 1;
							- value = 
							{ IPropertySubject 
								- _Name = "General";
								- Metaclasses = { IRPYRawContainer 
									- size = 1;
									- value = 
									{ IPropertyMetaclass 
										- _Name = "Graphics";
										- Properties = { IRPYRawContainer 
											- size = 1;
											- value = 
											{ IProperty 
												- _Name = "FitBoxToItsTextuals";
												- _Value = "False";
												- _Type = Bool;
											}
										}
									}
								}
							}
						}
					}
					- m_type = 87;
					- m_pModelObject = { IHandle 
						- _m2Class = "IClass";
						- _filename = "GameplayPkg.sbs";
						- _subsystem = "ModelPkg::GameplayPkg";
						- _class = "";
						- _name = "Game";
						- _id = GUID cacdcea2-8138-44e9-8da3-fe223d7b9d85;
					}
					- m_pParent = GUID 49ac931f-4968-4931-8298-9c9ac4a9a464;
					- m_name = { CGIText 
						- m_str = "Game";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 5;
					}
					- m_drawBehavior = 2056;
					- m_transform = 0.774806 0 0 0.751545 59.3003 -41.8531 ;
					- m_bIsPreferencesInitialized = 1;
					- m_AdditionalLabel = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 1;
					}
					- m_bFramesetModified = 1;
					- m_polygon = 4 2 329  2 1451  1061 1451  1061 329  ;
					- m_nNameFormat = 0;
					- m_nIsNameFormat = 0;
					- frameset = "<frameset rows=14%,86%>
<frame name=AttributeListCompartment>
<frame name=OperationListCompartment>";
					- Compartments = { IRPYRawContainer 
						- size = 2;
						- value = 
						{ CGICompartment 
							- _id = GUID 8dbf6bd0-e2de-4d06-8dd7-90f3be9c2cb3;
							- m_name = "Attribute";
							- m_displayOption = Explicit;
							- m_bShowInherited = 0;
							- m_bOrdered = 0;
							- Items = { IRPYRawContainer 
								- size = 0;
							}
						}
						{ CGICompartment 
							- _id = GUID 8e7fd1bb-f4e7-4b6e-912e-38dcc596a7a2;
							- m_name = "Operation";
							- m_displayOption = Explicit;
							- m_bShowInherited = 0;
							- m_bOrdered = 1;
							- Items = { IRPYRawContainer 
								- size = 6;
								- value = 
								{ IHandle 
									- _m2Class = "IPrimitiveOperation";
									- _filename = "GameplayPkg.sbs";
									- _subsystem = "ModelPkg::GameplayPkg";
									- _class = "Game";
									- _name = "addItsPlayer(Player)";
									- _id = GUID 84361919-eb9e-4320-b0fd-723252f94ddd;
								}
								{ IHandle 
									- _m2Class = "IPrimitiveOperation";
									- _filename = "GameplayPkg.sbs";
									- _subsystem = "ModelPkg::GameplayPkg";
									- _class = "Game";
									- _name = "doAction(char,int,int,int,int)";
									- _id = GUID 938fded3-e26d-4bc5-af89-8906134f0300;
								}
								{ IHandle 
									- _m2Class = "IPrimitiveOperation";
									- _filename = "GameplayPkg.sbs";
									- _subsystem = "ModelPkg::GameplayPkg";
									- _class = "Game";
									- _name = "getItsPlayer() const";
									- _id = GUID a01bb77a-f8d5-40b1-bd1d-fc2c8002dac9;
								}
								{ IHandle 
									- _m2Class = "IPrimitiveOperation";
									- _filename = "GameplayPkg.sbs";
									- _subsystem = "ModelPkg::GameplayPkg";
									- _class = "Game";
									- _name = "getItsVessel() const";
									- _id = GUID c059f635-35ce-459d-b221-9f1544b13c68;
								}
								{ IHandle 
									- _m2Class = "IPrimitiveOperation";
									- _filename = "GameplayPkg.sbs";
									- _subsystem = "ModelPkg::GameplayPkg";
									- _class = "Game";
									- _name = "newItsVessel(int,int,Player)";
									- _id = GUID f88a1357-ad27-4e4e-8df6-a75759eca8c8;
								}
								{ IHandle 
									- _m2Class = "IPrimitiveOperation";
									- _filename = "GameplayPkg.sbs";
									- _subsystem = "ModelPkg::GameplayPkg";
									- _class = "Game";
									- _name = "newPlayer(RhpString)";
									- _id = GUID dfc3a4f4-a565-4602-aba9-fd64a428a653;
								}
							}
						}
					}
					- Attrs = { IRPYRawContainer 
						- size = 0;
					}
					- Operations = { IRPYRawContainer 
						- size = 0;
					}
				}
				{ CGIPortConnector 
					- _id = GUID a082f01d-1028-4a19-bddc-29212f39514d;
					- m_type = 57;
					- m_pModelObject = { IHandle 
						- _m2Class = "IPort";
						- _filename = "SFMLInterfacePkg.sbs";
						- _subsystem = "SFMLInterfacePkg";
						- _class = "InterfaceManager";
						- _name = "ViewPort";
						- _id = GUID 3a23cbe5-9891-4555-8cfa-a64e283001ca;
					}
					- m_pParent = GUID 2120b671-3371-4b68-9b45-f826c8238cb2;
					- m_name = { CGIText 
						- m_str = "ViewPort";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nVerticalSpacing = -19;
						- m_nOrientationCtrlPt = 2;
					}
					- m_drawBehavior = 4096;
					- m_transform = -2.31381e-005 -4.08481 6.95687 -1.35858e-005 -270.722 1006.72 ;
					- m_bIsPreferencesInitialized = 1;
					- m_AdditionalLabel = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nVerticalSpacing = -29;
						- m_nOrientationCtrlPt = 2;
					}
					- m_position = 4 0 0  0 73  68 73  68 0  ;
					- m_pInheritsFrom = { IHandle 
						- _m2Class = "";
					}
					- m_nInheritanceMask = 0;
					- m_SubType = 0;
					- m_providedInterfaceLabel = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nHorizontalSpacing = 67;
						- m_nVerticalSpacing = -25;
						- m_nOrientationCtrlPt = 8;
					}
					- m_requiredInterfaceLabel = { CGIText 
						- m_str = "IGame";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nVerticalSpacing = 19;
						- m_nOrientationCtrlPt = 4;
					}
				}
				{ CGIPortConnector 
					- _id = GUID b6e65e9f-f814-4d62-bb14-b940ede41de4;
					- m_type = 57;
					- m_pModelObject = { IHandle 
						- _m2Class = "IPort";
						- _filename = "GameplayPkg.sbs";
						- _subsystem = "ModelPkg::GameplayPkg";
						- _class = "Game";
						- _name = "ModelPort";
						- _id = GUID 5e7f1d9f-13be-44ca-9f46-17331c14ba07;
					}
					- m_pParent = GUID d83deca8-4ed2-4ddd-b2e0-77ceb4482862;
					- m_name = { CGIText 
						- m_str = "ModelPort";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nVerticalSpacing = -19;
						- m_nOrientationCtrlPt = 0;
					}
					- m_drawBehavior = 0;
					- m_transform = 0 6.6893 -6.53756 0 1314.05 681.693 ;
					- m_bIsPreferencesInitialized = 0;
					- m_AdditionalLabel = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nVerticalSpacing = -29;
						- m_nOrientationCtrlPt = 0;
					}
					- m_position = 4 0 0  0 73  68 73  68 0  ;
					- m_pInheritsFrom = { IHandle 
						- _m2Class = "";
					}
					- m_nInheritanceMask = 0;
					- m_SubType = 0;
					- m_providedInterfaceLabel = { CGIText 
						- m_str = "IGame";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nVerticalSpacing = 19;
						- m_nOrientationCtrlPt = 6;
					}
					- m_requiredInterfaceLabel = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nVerticalSpacing = 19;
						- m_nOrientationCtrlPt = 6;
					}
				}
				{ CGIPortConnector 
					- _id = GUID 4b7374c7-c9a8-433d-b022-fc7e96bacef8;
					- m_type = 57;
					- m_pModelObject = { IHandle 
						- _m2Class = "IPort";
						- _filename = "ControllerPkg.sbs";
						- _subsystem = "ControllerPkg";
						- _class = "Controller";
						- _name = "ControllerPort";
						- _id = GUID 6e9de950-d21d-4c67-a9b1-1dc60d90674b;
					}
					- m_pParent = GUID 7eb51212-3a80-4c2e-864e-0ef65ea6efe8;
					- m_name = { CGIText 
						- m_str = "ControllerPort";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nVerticalSpacing = -19;
						- m_nOrientationCtrlPt = 0;
					}
					- m_drawBehavior = 4096;
					- m_transform = 0 3.85944 -5.72433 0 1282.25 489.229 ;
					- m_bIsPreferencesInitialized = 1;
					- m_AdditionalLabel = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nVerticalSpacing = -29;
						- m_nOrientationCtrlPt = 0;
					}
					- m_position = 4 0 0  0 73  68 73  68 0  ;
					- m_pInheritsFrom = { IHandle 
						- _m2Class = "";
					}
					- m_nInheritanceMask = 0;
					- m_SubType = 0;
					- m_providedInterfaceLabel = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nHorizontalSpacing = 67;
						- m_nVerticalSpacing = -25;
						- m_nOrientationCtrlPt = 8;
					}
					- m_requiredInterfaceLabel = { CGIText 
						- m_str = "IGame";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nVerticalSpacing = 19;
						- m_nOrientationCtrlPt = 6;
					}
				}
				{ CGIPackage 
					- _id = GUID 155a68d2-f1ca-4eef-a861-0497642a673a;
					- _properties = { IPropertyContainer 
						- Subjects = { IRPYRawContainer 
							- size = 1;
							- value = 
							{ IPropertySubject 
								- _Name = "General";
								- Metaclasses = { IRPYRawContainer 
									- size = 1;
									- value = 
									{ IPropertyMetaclass 
										- _Name = "Graphics";
										- Properties = { IRPYRawContainer 
											- size = 1;
											- value = 
											{ IProperty 
												- _Name = "FitBoxToItsTextuals";
												- _Value = "False";
												- _Type = Bool;
											}
										}
									}
								}
							}
						}
					}
					- m_type = 127;
					- m_pModelObject = { IHandle 
						- _m2Class = "ISubsystem";
						- _filename = "VesselPkg.sbs";
						- _subsystem = "ModelPkg";
						- _class = "";
						- _name = "VesselPkg";
						- _id = GUID 0228d9fd-4d84-44f9-bc23-6bcabf9424ee;
					}
					- m_pParent = GUID dca863ed-7e31-4997-a9ef-7555a6fe1110;
					- m_name = { CGIText 
						- m_str = "VesselPkg";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 5;
					}
					- m_drawBehavior = 4104;
					- m_transform = 0.869565 0 0 0.250639 52.8696 813.186 ;
					- m_bIsPreferencesInitialized = 1;
					- m_AdditionalLabel = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 1;
					}
					- m_polygon = 4 0 0  0 1151  1216 1151  1216 0  ;
					- m_nNameFormat = 0;
					- m_nIsNameFormat = 0;
					- Compartments = { IRPYRawContainer 
						- size = 0;
					}
				}
				{ CGIPackage 
					- _id = GUID fc3def00-68eb-4188-b1eb-3ee27c726af6;
					- _properties = { IPropertyContainer 
						- Subjects = { IRPYRawContainer 
							- size = 1;
							- value = 
							{ IPropertySubject 
								- _Name = "General";
								- Metaclasses = { IRPYRawContainer 
									- size = 1;
									- value = 
									{ IPropertyMetaclass 
										- _Name = "Graphics";
										- Properties = { IRPYRawContainer 
											- size = 1;
											- value = 
											{ IProperty 
												- _Name = "FitBoxToItsTextuals";
												- _Value = "False";
												- _Type = Bool;
											}
										}
									}
								}
							}
						}
					}
					- m_type = 127;
					- m_pModelObject = { IHandle 
						- _m2Class = "ISubsystem";
						- _filename = "ObjectPkg.sbs";
						- _subsystem = "SFMLInterfacePkg";
						- _class = "";
						- _name = "ObjectPkg";
						- _id = GUID 3754b813-989e-4b9e-a5ae-3433ff9aa677;
					}
					- m_pParent = GUID 682782bf-e35b-4bc7-a416-2abcf7467f12;
					- m_name = { CGIText 
						- m_str = "ObjectPkg";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 5;
					}
					- m_drawBehavior = 4104;
					- m_transform = 0.361111 0 0 0.38619 709.333 94.5636 ;
					- m_bIsPreferencesInitialized = 1;
					- m_AdditionalLabel = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 1;
					}
					- m_polygon = 4 0 0  0 1151  1216 1151  1216 0  ;
					- m_nNameFormat = 0;
					- m_nIsNameFormat = 0;
					- Compartments = { IRPYRawContainer 
						- size = 0;
					}
				}
				{ CGIPackage 
					- _id = GUID eb35dc5f-bcdd-48a0-b4eb-5e3006026615;
					- _properties = { IPropertyContainer 
						- Subjects = { IRPYRawContainer 
							- size = 1;
							- value = 
							{ IPropertySubject 
								- _Name = "General";
								- Metaclasses = { IRPYRawContainer 
									- size = 1;
									- value = 
									{ IPropertyMetaclass 
										- _Name = "Graphics";
										- Properties = { IRPYRawContainer 
											- size = 1;
											- value = 
											{ IProperty 
												- _Name = "FitBoxToItsTextuals";
												- _Value = "False";
												- _Type = Bool;
											}
										}
									}
								}
							}
						}
					}
					- m_type = 127;
					- m_pModelObject = { IHandle 
						- _m2Class = "ISubsystem";
						- _filename = "ViewPkg.sbs";
						- _subsystem = "SFMLInterfacePkg";
						- _class = "";
						- _name = "ViewPkg";
						- _id = GUID 4767c02b-b9cd-491a-aeb0-583a352a10a3;
					}
					- m_pParent = GUID 682782bf-e35b-4bc7-a416-2abcf7467f12;
					- m_name = { CGIText 
						- m_str = "ViewPkg";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 5;
					}
					- m_drawBehavior = 4104;
					- m_transform = 0.361111 0 0 0.38619 709.333 589.112 ;
					- m_bIsPreferencesInitialized = 1;
					- m_AdditionalLabel = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 1;
					}
					- m_polygon = 4 0 0  0 1151  1216 1151  1216 0  ;
					- m_nNameFormat = 0;
					- m_nIsNameFormat = 0;
					- Compartments = { IRPYRawContainer 
						- size = 0;
					}
				}
				
				- m_access = 'Z';
				- m_modified = 'N';
				- m_fileVersion = "";
				- m_nModifyDate = 0;
				- m_nCreateDate = 0;
				- m_creator = "";
				- m_bScaleWithZoom = 1;
				- m_arrowStyle = 'S';
				- m_pRoot = GUID ebdfea3a-d71e-485f-8ceb-2e6e1cd725f9;
				- m_currentLeftTop = 0 0 ;
				- m_currentRightBottom = 0 0 ;
			}
			- _defaultSubsystem = { IHandle 
				- _m2Class = "";
			}
		}
	}
	- Components = { IRPYRawContainer 
		- size = 1;
		- value = 
		{ IComponent 
			- fileName = "Test";
			- _id = GUID 5c2d95e6-0d15-4ad0-b2f0-fa2a53b3a0c0;
		}
	}
}

