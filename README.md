Battleship Deluxe Pro Extreme
=============================

This project aims to create a *turn based multiplayer strategy game*:
Battleship Deluxe Pro Extreme.
This is a mix between *Battleship* and a regular strategy game.
The game has the following features:

  - Support for two or more players (*multiplayer*),
    either networked or hot-seat style.
  - Players do their actions in turns (*turn based*).
  - Players can take a number of actions---like shooting or moving---every turn,
    giving the game a *strategy element*.

Like in *Battleship*, players have to start by positioning their pieces (ships) on the board.
After this initial turn, players can take a number of *actions*.

This game is being developed with IBM's Rational Rhapsody as part of the Advanced Programming course at the University of Twente.

The project is licensed under the EUPL version 1.1 or higher.
Builds may includes external software under different (included) licences:

  - DejaVu Sans Mono (Bitstream Vera Fonts Copyright, Arev Fonts Copyright, Public Domain)
  - SFML (zlib License)

Rhapsody and Git
----------------
Using Rhapsody with Git has some peculiarities.
The file `.gitconfig` contains some settings that one wants
(including the parameters for the Rhapsody's DiffMerge).
Enable it with:

    git config --local include.path ../.gitconfig

To use DiffMerge, use Git's `mergetool` and `difftool` commands with `-t diffmerge`.
Make sure a `diffmerge` command exists that points to Rhapsody's `DiffMerge.exe`, either via symbolic link or bash alias:

    alias diffmerge="C:/Program Files (x86)/IBM/Rational/Rhapsody/8.1.0/DiffMerge.exe"


Rhapsody and Linux
------------------
For Debian Jessie, the dependencies are:

    libx11-6 libxext6 build-essential binutils rpm libstdc++5

The CLI installation can be started with:

    ./disk1/im/installer.linux.gtk.x86/consoleinst.sh

This guides you through the entire installation.
The defaults are usually correct.

Make sure `/etc/hosts` contains the hostname of the computer for IPv4.

Required commands for CI can be installed with the `rhapsody_linux.sh` script in the `scripts` directory.
