#!/usr/bin/env bash

comp="$1"
conf="$2"
shift 2

# Dependencies
DEPS="fonts"
WINDEPS="../../SFML-2.4.1/bin/*.dll"

# Directories
DIR="$(dirname "$0")"
BUILD_DIR="${comp}/${conf}"

# Change directory
cd "${DIR}/rhapsody"

# Create build directory
mkdir -p "${BUILD_DIR}"

# Set correct dependencies
case "$conf" in
    "Windows")     DEPS="$DEPS $WINDEPS";;
    "VesselDebug") DEPS="$DEPS $WINDEPS";;
esac

# Copy dependencies
for dep in ${DEPS}; do
    [[ -e "${dep}" ]] && cp -r "${dep}" "${BUILD_DIR}/"
done

# Exit if only dependencies are required
[[ "$1" = "-deps" ]] && exit 0

# Run rhapsody and generate code
rhapsodycl -cmd=open BattleshipDeluxeProExtreme.rpy \
	 -cmd=setcomponent "${comp}" \
	 -cmd=setconfiguration "${conf}" \
	 -cmd=regenerate \
	 "$@"
